<?php
require_once 'conf.php';

function installer($servername, $username ,$password, $dbname )	{
	
try {
	$conn = new PDO("mysql:host=$servername", $username, $password);
    
    // set the PDO error mode to exception
    
	  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  $drop="DROP DATABASE IF EXISTS ".$dbname;
    $conn->exec($drop);
    
    //creating databse

	  $createdb="CREATE DATABASE ".$dbname;
    $usedb="USE ".$dbname;
    
    //executing query

	  $conn->exec($createdb);
    $conn->exec($usedb);
    
    //Table creation

	  $table1="CREATE TABLE spost (
		idn int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		title  varchar(50) NOT NULL,
		content text NOT NULL,
    date  varchar(25) NOT NULL) ";
     
    $conn->exec($table1);	 
    
  	$table2="CREATE TABLE tags (
		tid  int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		tag varchar(100) NOT NULL) ";

    $conn->exec($table2);	 		 

	  $table3="CREATE TABLE relation (
		tag_id  int(11) NOT NULL,
		blog_id int(11) NOT NULL,
    FOREIGN KEY (blog_id) REFERENCES spost (idn),
    FOREIGN KEY (tag_id) REFERENCES tags (tid))";
    
    $conn->exec($table3);
}
catch(PDOException $e)  {
	  echo "Connection failed: " . $e->getMessage();
	}
	
		return $conn;
		
}

function random_inserter($conn,$dummycurrentdate,$dummyt,$dummyb,$dummycurrentdate,$s,$words)	{
	$insert1 = "INSERT INTO spost (title, content, date) VALUES ('$dummyt', '$dummyb', '$dummycurrentdate')";
        $conn->exec($insert1);
  
        for($i=0;$i<$s;$i++)  {
          $resl = $conn->query("SELECT  tag FROM tags  WHERE  tag='$words[$i]'");
          $count = $resl->rowCount();    
          
          if($count==0) {
            $tagsq="INSERT INTO tags (tag)  VALUES('$words[$i]')";
            $conn->exec($tagsq);
          }  
          $relquery1="INSERT into relation(tag_id,blog_id) VALUES
          ((select tid from tags where tag = '$words[$i]'),
          (select idn from spost where title = '$dummyt'))";

          $conn->exec($relquery1);
	
}
}
//Random word generator
function gen($n)	{
	$b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
            'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
            'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
            'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
            'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
            'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
            'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
            'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
            'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
            'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
            'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
            'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
            'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
            'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
            'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
            'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
            'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
            'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
            'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
            'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
            'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
            'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
            'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
            'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
            'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
            'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
            'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
            'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
            'vulputate');
        
	$rand=array(); 
	shuffle($b);
	for($i=0;$i<$n;$i++)	{
		$rand[$i]=$b[$i];  
	}
	$rand= implode(" ",$rand);
	return $rand;
}
 
//connecting database
function db_connect()	{
  global $servername;
	global $dbname;
	global $username;
	global $password;
	static $db;	
  if(!isset($db))	{
		try {
   		$db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    	// set the PDO error mode to exception
    	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	//echo "Connected successfully";
  	}
		catch(PDOException $e)	{
    	echo "Connection failed: " . $e->getMessage();
		}
	} 
return $db;
} 
    
function postadder($conn,$title,$blog,$d,$tag)	{
				$tag=trim($tag," ");
        $tag=strtolower($tag);
        $wordsarray= explode(",",$tag);
	      $s=sizeof($wordsarray);
        $blog = addslashes($blog);
        for($i=0;$i<$s;$i++)
        {
            $wordsarray[$i]=trim($wordsarray[$i]);
        }
	        try
          {  
            $sql = "INSERT INTO spost (title, content, date) VALUES ('$title', '$blog', '$d')";
            $conn->exec($sql);
            for($i=0;$i<$s;$i++)
            {
							
              $tagurl="SELECT  tag FROM tags  WHERE  tag='$wordsarray[$i]'";
              $result = $conn->query($tagurl);
              $result->setFetchMode(PDO::FETCH_ASSOC);
              //$result=$conn->exec($tagrl);
              $count = $result->rowCount();    
              // $wordsarray[$i]=trim($wordsarray[$i]);
              if($count==0)
              {
                $tagsq="INSERT INTO tags (tag)  VALUES('$wordsarray[$i]')";
                $conn->exec($tagsq);
              }  
              //insertion to 3rd table, i.e the relation table
              $relquery="INSERT into relation(tag_id,blog_id) VALUES
              ((select tid from tags where tag = '$wordsarray[$i]'),
              (select idn from spost where title = '$title'))";
              $conn->exec($relquery);
              //end of insertion to relation table
              header("location:index.php");
            }
         
         
            echo "New record created successfully";
          }
          catch(PDOException $e)
          {
            echo "Connection failed: " . $e->getMessage();
          }
}

function postupdater($conn,$blog,$title,$id,$tag)	{
	try
          {  
		       $tag=trim($tag," ");
           $tag=strtolower($tag);
           $wordsarray= explode(",",$tag);
           $s=sizeof($wordsarray);
		
           for($i=0;$i<$s;$i++)
          {
            $wordsarray[$i]=trim($wordsarray[$i]);
          }
            $blog = addslashes($blog);
            $sql = "UPDATE spost SET title='$title',content='$blog' WHERE idn='$id'" ;
            $conn->exec($sql);
            $reldel="DELETE from relation where blog_id='$id'";
            $conn->exec($reldel);

            for($i=0;$i<$s;$i++)
            {
              $tagurl="SELECT  tag FROM tags  WHERE  tag='$wordsarray[$i]'";
              $result = $conn->query($tagurl);
              $result->setFetchMode(PDO::FETCH_ASSOC);
              //$result=$conn->exec($tagrl);
              $count = $result->rowCount();    
              // $wordsarray[$i]=trim($wordsarray[$i]);
              if($count==0)
              {
                $tagsq="INSERT INTO tags (tag)  VALUES('$wordsarray[$i]')";
                $conn->exec($tagsq);
              }  
              //insertion to 3rd table, i.e the relation table
              $relquery="INSERT into relation(tag_id,blog_id) VALUES
              ((select tid from tags where tag = '$wordsarray[$i]'),
              (select idn from spost where title = '$title'))";
              $conn->exec($relquery);
              $posted = true;
              //end of insertion to relation table
              //header("location:adpost.php");
            }
            echo "New record created successfully";
            Header("Location: post.php?id=$id&posted=$posted");
        }
        catch(PDOException $e)
        {
          echo "Connection failed: " . $e->getMessage();
        }
}


//displays contents in post page
function postdisplayer($id,$conn)	{
	$stmt = $conn->prepare("SELECT  title,content,date FROM  spost WHERE idn=?");
	$stmt->execute([$id]); 
  return 	$stmt->fetch();
}
//displays contents in index page
function allpostdisplayer($conn , $offset ,$sort , $n)	{
$stmt = $conn->prepare("SELECT idn,title,content,date FROM spost ORDER BY idn $sort LIMIT $offset,$n");
$stmt->execute();
$data = $stmt->fetchAll();
return $data;	
}

function tagpostdisplayer($conn,$tagidvalue2,$offset,$n,$sort2)	{
	
$sql = "SELECT idn,title,content,date FROM spost,relation 
where spost.idn=relation.blog_id and $tagidvalue2 = relation.tag_id 
ORDER BY idn $sort2 LIMIT $offset,$n";
                   
if(filter_var($tagidvalue2, FILTER_VALIDATE_INT))	{
	$stmt = $conn->prepare($sql); 
	$stmt->execute();
	$data = $stmt->fetchAll();
}
return $data;	
}

function tagdisplayer($id,$conn)	{
	$stmt2=$conn->prepare("SELECT tags.tag,tags.tid FROM relation,tags WHERE relation.blog_id = ? AND tags.tid=relation.tag_id");
	$stmt2->execute([$id]);
	return $stmt2->fetchAll();
}

function pagecounter($n,$conn)	{
	$total_pages_sql = "SELECT idn FROM spost";
  $q1 = $conn->query($total_pages_sql);
  $total_rows = $q1->rowCount();
  $total_pages = ceil($total_rows / $n);
	return $total_pages;
}
//for tagged page
function pagecounter2($n,$tagidvalue2,$conn)	{
	$q1 = $conn->query("SELECT idn FROM spost,relation
  where spost.idn=relation.blog_id and $tagidvalue2 = relation.tag_id");
	$total_rows = $q1->rowCount();
	$total_pages = ceil($total_rows / $n);
	return $total_pages;
}

function content_trimmer($str)	{
	$words = explode(" ",$str);
	$cont =  implode(" ", array_splice($words, 0, 200));
	if(str_word_count($cont)>199)	{
		$cont= $cont."...";
	}
	return $cont;
}


?>