<?php
if(!is_file("conf.php"))
{
  header("location:install.php");
}
?>


<!DOCTYPE html>
<html lang="en">
 
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 
  <title>Clean Blog - Start Bootstrap Theme</title>
 
  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
 
  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
 
  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
 
</head>
 
<body>



 
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Test Post</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="adpost.php">Add Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
 
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>


 
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <!-- <div class="dropdown" >
          <button type="submit" class="btn btn-primary float-right ralign" name= "sort" id="sort">sort</button>
        </div> -->
        <?php
           if (isset($_GET['pageno'])) {
           $pnoi = $_GET['pageno'];
            } else {
            $pnoi = 1;
             }
        ?>


        <form action="" method="GET">
        <input type="hidden" name="pageno" value="<?php echo $pnoi; ?>">
        <select name="sort" id="srt" onchange="this.form.submit()">
        <option value="null" disabled selected>Sort</option>
        <option value="ASC">Sort by Asc</option>
        <option value="DESC">Sort by Desc</option>
        </select>
        </form>
         
         
                    <?php
                     if(isset($_GET["sort"])){
                      $sort=$_GET["sort"];
                      echo "select sort is => ".$sort;
                      }
                      else{
                        $sort="DESC";
                      }


                    require 'conf.php';
 
                    if (isset($_GET['pageno'])) {
                      $pageno = $_GET['pageno'];
                    } else {
                      $pageno = 1;
                    }
 
                    $n = 10;
                    $offset = ($pageno-1) * $n;
                    $total_pages_sql = "SELECT idn FROM spost";
                    $q1 = $conn->query($total_pages_sql);
                    $total_rows = $q1->rowCount();
                    $total_pages = ceil($total_rows / $n);
                    // $sql = "SELECT * FROM BlogDetails order by id desc LIMIT $offset,$n";
                    // $q2 = $conn->query($sql);
                    // $q2->setFetchMode(PDO::FETCH_ASSOC);
 
 
                    //$id=1;
                    $stmt = $conn->prepare("SELECT idn,title,content,date FROM spost ORDER BY idn $sort LIMIT $offset,$n");
                    $stmt->execute();
                    $data = $stmt->fetchAll();
 
                    if (isset($data)) {
                      // output data of each row
                      foreach ($data as $row) {
                        $val=$row["idn"];
                        $str=$row["content"];
                        $words = explode(" ",$str);
                        $cont =  implode(" ", array_splice($words, 0, 200));
                        if(str_word_count($cont)>199){
                          $cont= $cont."...";
                        }
                       
                       
                       
                        echo '
                        <div class="post-preview">
                          <a href="post.php?id='.$val.'">
                            <h2 class="post-title">'.$row["title"].'</h2>
                            <h3 class="post-subtitle">'.$cont.'</h3>
                          </a><p class="post-meta">Posted by
                        <a href="#">Start Bootstrap</a>
                          on '.$row["date"].'</p>
                         
                        ';
 
                        $sql1="SELECT tags.tag,tags.tid FROM relation,tags WHERE relation.blog_id = ? AND tags.tid=relation.tag_id";
                        $stmt2 = $conn->prepare($sql1);
                        $stmt2->execute([$val]);
                        $data2 = $stmt2-> fetchAll();
                        echo "<p>Tags: ";
                        if (isset($data2)) {
                          foreach ($data2 as $row2) {
                          $tagidval=$row2["tid"];
                          echo '<a href="tagged.php?tag='.$tagidval.'">#'.$row2["tag"].' </a>';
                               
                          }
                        }
                        echo "</p>
                        </div>
                        <hr>";
                      }
                    } else {
                      echo "0 results";
                  }
                 
                  $conn = null;
                    ?>
 
        <!-- Pager -->
        <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if($pageno == 1){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if($pageno == 1){ echo '#'; } else { echo "index.php?pageno=".($pageno - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if($pageno == $total_pages){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if($pageno == $total_pages){ echo '#'; } else { echo "index.php?sort=$sort&pageno=".($pageno + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>
      </div>
    </div>
  </div>
 
  <hr>
 
  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>
 
  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
 
  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
 
</body>
 
</html>